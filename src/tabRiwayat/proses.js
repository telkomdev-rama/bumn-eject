import React from "react";
import { StatusBar, Image, MapView, AsyncStorage, Alert, TouchableOpacity } from "react-native";
import {
	Button,
	Text,
	Container,
	Card,
	CardItem,
	Body,
	Content,
	Header,
	Title,
	Left,
	Icon,
	Right,
	Footer,
	FooterTab,
	H1, H2,
	Thumbnail,
	Form,
	Item,
	Label,
	Input,
	List,
	ListItem,
	View

} from "native-base";

import Modal from 'react-native-modal';
import DetailLaporanSelesai from "./detailLaporanSelesai.js";

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';


export default class Proses extends React.Component {

	constructor(props) {
		super(props)
		this.state = {
			dataAktif: [],
			dataSewaAktif: [],
			dataBarang: [],
			dataSewaAktifDetail: [],
			username: "",
			dataProses : [],
		}
	}


	componentDidMount() {
		 AsyncStorage.getItem("bumnId", (error, result) => {
		 fetch("http://ec2-13-250-62-76.ap-southeast-1.compute.amazonaws.com:7000/api/v1/laporan/byStatusProses", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                instansi : result
            })
        })
            .then(response => response.json())
            .then((data) => {
                console.log(data.response);
                this.setState({
							dataProses: data.response

						});
						console.log(this.state.dataProses)

            })
		 })

	}


	render() {
		//const { navigate } = this.props.navigation;
		
		return (
			<Container>

				<Content>
					<Card style={{ backgroundColor: 'powderblue', marginRight: "5%", marginTop: "5%", marginLeft: "5%" }}>
						<CardItem style={{ backgroundColor: 'powderblue' }}>
							<Left>
								<Thumbnail square small
									source={require("../../img/asset/ic-lamp.png")}
								/>
								<Body>
									<Text style={{ fontSize: responsiveFontSize(1.5), fontStyle: "italic", marginLeft: "10%" }}>Anda dapat mengupdate status laporan kapanpun dengan mengupload eviden pengerjaan.</Text>

								</Body>
							</Left>
						</CardItem>
					</Card>

 {
							this.state.dataProses.map((item, index) => (
					<List>
						

						
						<ListItem  style={{borderBottomWidth: 0}}>
<TouchableOpacity key={item.nomor_laporan}>
							<Left>
								<Thumbnail small
									source={{uri : item.images}}
								/>
								<Body>
									<Text style={{ fontSize: responsiveFontSize(2) }}>{item.pelapor}</Text>
									<Text style={{ fontSize: responsiveFontSize(1.5) }}>No. Tiket : {item.nomor_laporan}</Text>
								</Body>
							</Left>
							
							<Right>
								<Text style={{ fontSize: responsiveFontSize(1.5),fontStyle:"italic" }}>{item.tanggal_laporan}</Text>
							</Right>
							</TouchableOpacity>
					
						</ListItem>
						
						
						<ListItem style={{marginTop:"-5%",marginRight:"5%"}} >
							<Text style={{fontSize: responsiveFontSize(2)}}>{item.description}</Text>
						</ListItem>
						
						
					</List>
					 ))
						} 
					
					

					

					
				</Content>
			</Container>
		);
	}



}



