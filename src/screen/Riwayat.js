import React from "react";
import { AppRegistry, View,StyleSheet,  StatusBar } from "react-native";
import {
  Button,
  Text,
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Left,
  Right,
  Icon,
  Title,
  Input,
  InputGroup,
  Item,
  Tab,
  Tabs,
  Footer,
  FooterTab,
  Label,
  Thumbnail
} from "native-base";

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

export default class Riwayat extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}
            >
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>Jade Chat</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Item floatingLabel style={{ marginTop: 20 }}>
            <Label>Jade Chat</Label>
            <Input />
          </Item>
          <Button
            rounded
            info
            style={{ marginTop: 20, alignSelf: "center" }}
            onPress={() => navigate("Profile")}
          >
            <Text>Goto Jade Profile</Text>
          </Button>
        </Content>

         <Footer>
                    <FooterTab style={styles.footer}>
                        <Button
                            onPress={() => this.props.navigation.navigate("Beranda")}
                        >

                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/ic-home-biru.png")}
                            />

                            <Text uppercase={false} style={styles.fontFooter}>Beranda</Text>
                        </Button>
                        <Button
                            onPress={() => this.props.navigation.navigate("Riwayat")}
                        >
                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/ic-riwayat-abu.png")}
                            />
                            <Text uppercase={false} style={styles.fontFooter}>Riwayat</Text>
                        </Button>
                        <Button
                            vertical
                            onPress={() => this.props.navigation.navigate("Bantuan")}
                        >
                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/ico-help.png")}
                            />
                            <Text uppercase={false} style={styles.fontFooter}>Bantuan</Text>
                        </Button>
                        <Button
                            onPress={() => this.props.navigation.navigate("Akun")}
                        >
                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/ic-profil-abu.png")}
                            />
                            <Text uppercase={false} style={styles.fontFooter}>Akun</Text>
                        </Button>
                    </FooterTab>
                </Footer>
      </Container>
    );
  }
}


const styles = StyleSheet.create({
    imgFooter: {
        height: "40%",
        width: "22%",
        marginBottom: "5%"
    },
    footer: {
        backgroundColor: "#fff"
    },
    fontFooter: {
        color: "#000",
        fontSize: responsiveFontSize(1.5)
    }
});
