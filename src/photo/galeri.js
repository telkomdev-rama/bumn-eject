import React from "react";
import { StatusBar, Image, MapView, AsyncStorage, Alert, TouchableOpacity} from "react-native";
import {
    Button,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Footer,
    FooterTab,
    H1,H2,
    Thumbnail,
    Form,
    Item,
    Label,
    Input,
	List,
	ListItem,
	View

} from "native-base";

import { StyleSheet } from 'react-native';
import { ImagePicker } from 'expo';

import Modal from 'react-native-modal';



export default class Galeri extends React.Component {

   

	 state = {
       image: null,
   };

   render() {
       let { image } = this.state;

       return (
           <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
               <Button block primary
                   onPress={this._pickImage}
               >
			   <Text>OK</Text>
			   </Button>
               {image &&
                   <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
           </View>
       );
   }

   _pickImage = async () => {
       let result = await ImagePicker.launchImageLibraryAsync({
           allowsEditing: true,
           aspect: [4, 3],
       });

       console.log(result);

       if (!result.cancelled) {
           this.setState({ image: result.uri });
       }
   }; 
    

}





